<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Post;
use App\Models\Student;

class HomeController extends Controller
{
    protected $User;
    protected $Post;
    protected $Student;

    public function __construct()
    {
        $this->User = new User();
        $this->Post = new Post();
        $this->Student = new Student();
    }


    function home(){
        // return view('home');
        $all_user = $this->User->get_all();
        $all_post = $this->Post->get_post();
        $all_student_course = $this->Student->get_all();
        return view('home')->with('all_user', $all_user)->with('all_post', $all_post)->with('all_student_course', $all_student_course);
    }
    
}
