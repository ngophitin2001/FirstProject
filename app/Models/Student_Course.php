<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student_Course extends Model
{
    protected $table = 'Student_Course';
    public $timestamps = false; 
    
    use HasFactory;
}
