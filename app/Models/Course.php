<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'course';
    protected $primaryKey = 'idCourse';
    use HasFactory;

    public function students()
    {
        return $this->belongsToMany(User::class, 'Student_Course', 'idCourse', 'idStudent');
    }
}
