<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'student';
    protected $primaryKey = 'idStudent';


    use HasFactory;

    public function coursessss()
    {
        return $this->belongsToMany('App\Models\Course', 'Student_Course', 'idStudent', 'idCourse');
    }

    public function get_all()
    {
        return self::with('coursessss')->get();
        // return self::with('coursessss')->where(['idStudent' => 1])->get();
        // return self::with('coursessss')->orderBy('idStudent')->get();
        // return self::with('coursessss')->orderByDesc('idStudent')->get();
        // return self::with('coursessss')->limit(1)->get();
        // return self::with('coursessss')->groupBy('idStudent', 'student.Name')->get();
        // return self::with('coursessss')->distinct()->get();
        // return self::with('coursessss')->join('tenbang2', 'tenbang1.id', '=', 'tenbang2.id')->get();

        // sử dụng whereHas khi bạn muốn áp dụng điều kiện cho một mối quan hệ (relationship) của mô hình đó.
        // whereHas chỉ áp dụng cho model gốc - nghĩa là ở model nào thì dùng wherehas cho model đó - như ở đây thì dùng với course không được mà phải dùng với student
        // return self::with('coursessss')->whereHas('coursessss', function ($query) {
        //     $query->where('student.idStudent', 1);
        // })->get();

        //return self::withCount('coursessss')->get(); // sẽ có thêm trường coursessss_count cho model gốc


        // select -> chỉ select được với những dữ liệu của bảng student
        // return self::with('coursessss')->select('student.*')->get();

        //test rebase chính thức lklklklkslakldadsgit
    }
}
