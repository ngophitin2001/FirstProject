<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    public function comments()
    {
        return $this->hasMany('App\Models\Comment', 'idPost', 'idPost');
    }

    public function get_post()
    {
        return Post::with('comments')->get();
    }
}
