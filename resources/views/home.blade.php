@extends('layout')
@section('content')
<div class="body__header">
	<div id="banner">
        HOME
	</div>
	<div id="body__show-category">
		<div class="container body__show-category__wrap">
            <h1>1-1</h1>
			@foreach($all_user as $key => $user)
			    <?php 
                    echo $key ."=>.". $user;
                ?> 
			@endforeach
		</div>

        <div class="container body__show-category__wrap">
            <h1>1-n</h1>
            <h6>1 post nhiều user comment</h6>
			@foreach($all_post as $key => $comment)
			    <?php 
                    echo $key ."=>.". $comment;
                ?> 
			@endforeach
		</div>

        <div class="container body__show-category__wrap">
            <h1>n-n</h1>
            <h6>1 student có nhiều course và 1 source có nhiều student</h6>
			@foreach($all_student_course as $key => $comment)
			    <?php 
                    echo $key ."=>.". $comment;
                ?> 
			@endforeach

			<br/>
			<br/>

			<!-- @ foreach($all_student_course as $student)
				< ?php
					echo "Sinh viên: {$student->Name}\n";
				?>
				
				@ foreach ($student->coursessss as $course)
					< ?php echo " - Khóa học: {$course->Name}\n"; ?>
				@ endforeach
			@ endforeach -->
		</div>

	</div>
</div>

@endsection